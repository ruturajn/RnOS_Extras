# Procedure for building Custom ARCH ISO

- Install `archiso` and `devtools` using `pacman`.

- Navigate to your custom ISO folder and copy `releng` there, with,
    `$ cp -r /usr/share/archiso/configs/releng/ .`

- Edit `packages.x86_64` to include the packages you require from `pacman`.

- To use the packages that are in the AUR, we need to create a custom local
    repository.

- Add Local custom repository to `pacman.conf`:
    - For this, first get the `PKGBUILD`, for the package from the AUR.
    - We need to build any package that is included in the ISO in a clean chroot, so there are no missing dependencies.
    - The script named `build_package.sh`, will handle this. It creates a containerized Arch environment, and builds the
        packages in it. Run this script, from the directory that contains the `PKGBUILD`.
    - Now, instead of running the script above for every single package and manually getting the required sources from the AUR
        for each package, we can wrap the script above in `get_packages.sh`, which will automate the whole process.
    - For the packages not in the AUR, you will need to create your own `PKGBUILDs`. The protoptype for a `PKGBUILD`
        exists on your system in `/usr/share/pacman/` named `PKGBUILD.proto`. You can also find other useful things in
        that directory.

- Note: Use `repo-remove <db_name>.db.tar.gz <package_name>` to remove a
package from the database.

- The config files for qtile, rofi, fish, etc. need to be placed in `<custom_ISO_folder>/airootfs/etc/skel/.config/`.
So, instead, of directly putting everything in the `<custom_ISO_folder>/airootfs/etc/skel/` directory, we can
package it up. Also, then everything that you would like to include in the home folder of the live user, can be
placed there. In this case, I have packaged all the configuration files in the [RnOS_System_Config](https://gitlab.com/ruturajn/RnOS_System_Config) repository.

- Now, to login to a display manager, add and enable its service via systemd.
```
$ ln -s /usr/lib/systemd/system/sddm.service <custom_ISO_folder>/airootfs/etc/systemd/system/display-manager.service
```

- `NetworkManager` and `bluetooth` services can be enabled with (make sure you change the `<custom_ISO_folder>` value),
```
$ ln -s /usr/lib/systemd/system/NetworkManager-wait-online.service <custom_ISO_folder>/airootfs/etc/systemd/system/network-online.target.wants/NetworkManager-wait-online.service
$ ln -s /usr/lib/systemd/system/NetworkManager-dispatcher.service <custom_ISO_folder>/airootfs/etc/systemd/system/dbus-org.freedesktop.nm-dispatcher.service
$ ln -s /usr/lib/systemd/system/NetworkManager.service <custom_ISO_folder>/airootfs/etc/systemd/system/multi-user.target.wants/NetworkManager.service

$ mkdir <custom_ISO_folder>/airootfs/etc/systemd/system/bluetooth.target.wants/
$ ln -s /usr/lib/systemd/system/bluetooth.service <custom_ISO_folder>/airootfs/etc/systemd/system/bluetooth.target.wants/bluetooth.service
$ ln -s /usr/lib/systemd/system/bluetooth.service <custom_ISO_folder>/airootfs/etc/systemd/system/dbus-org.bluez.service
```

- `Pulseaudio` service for the user can be enabled with,
```
$ ln -s /usr/lib/systemd/user/pulseaudio.socket <custom_ISO_folder>/airootfs/etc/skel/.config/systemd/user/sockets.target.wants/pulseaudio.socket
$ ln -s /usr/lib/systemd/user/pulseaudio.service <custom_ISO_folder>/airootfs/etc/skel/.config/systemd/user/default.target.wants/pulseaudio.service
```

- For `avahi-daemon` service,
```
$ ln -s /usr/lib/systemd/system/avahi-daemon.service <custom_ISO_folder>/airootfs/etc/systemd/system/sockets.target.wants/avahi-daemon.socket
$ ln -s /usr/lib/systemd/system/avahi-daemon.service <custom_ISO_folder>/airootfs/etc/systemd/system/multi-user.target.wants/avahi-daemon.service
$ ln -s /usr/lib/systemd/system/avahi-daemon.service <custom_ISO_folder>/airootfs/etc/systemd/system/dbus-org.freedesktop.Avahi.service
```

- Finally, for the `cups` service,
```
$ ln -s /usr/lib/systemd/system/cups.path <custom_ISO_folder>/airootfs/etc/systemd/system/multi-user.target.wants/cups.path
$ ln -s /usr/lib/systemd/system/cups.socket <custom_ISO_folder>/airootfs/etc/systemd/system/sockets.target.wants/cups.socket
$ ln -s /usr/lib/systemd/system/cups.service <custom_ISO_folder>/airootfs/etc/systemd/system/multi-user.target.wants/cups.service
$ ln -s /usr/lib/systemd/system/cups.service <custom_ISO_folder>/airootfs/etc/systemd/system/printer.target.wants/cups.service
```

- Add the `sddm-candy-theme`, to `sddm`, by creating a file in `<custom_ISO_folder>/airootfs/etc/sddm.conf.d/default.conf`,
and put the following in it,
```
[Theme]
Current=Sugar-Candy
CursorTheme=Sunity-cursors

[Autologin]
# Whether sddm should automatically log back into sessions when they exit
Relogin=false

# Name of session file for autologin session (if empty try last logged in)
Session=qtile

# Username for autologin session
User=rnos
```

- If you do not want the user to be auto-logged-in, you can place a state file named `state.conf`
in `<custom_ISO_folder>/airootfs/var/lib/sddm`, with the following contents,
```
[Last]
# Name of the last logged-in user.
# This user will be preselected when the login screen appears
User=rnos


# Name of the session for the last logged-in user.
# This session will be preselected when the login screen appears.
Session=/usr/share/xsessions/qtile.desktop
```

- To create a user, add these lines to `<custom_ISO_folder>/airootfs/etc/passwd`,
```
<your_preferred_username>:x:1000:1000::/home/<your_preferred_username>:/usr/bin/zsh
```
In this case,
```
root:x:0:0:root:/root:/usr/bin/zsh
rnos:x:1000:1000::/home/rnos:/usr/bin/fish
```

- Then, generate a password hash with `$ openssl passwd -6`, and add it to `<custom_ISO_folder>/airootfs/etc/shadow`.
> (Note: Password used is "rnos", and <your_preferred_username> = rnos)

- Now, add user's group to `<custom_ISO_folder>/airootfs/etc/group`. The following, shows the contents
added to `<custom_ISO_folder>/airootfs/etc/group`.
```
root:x:0:root
adm:x:4:rnos
wheel:x:10:rnos
uucp:x:14:rnos
rnos:x:1000:
video:x:985:sddm,rnos
sys:x:3:bin,rnos
network:x:90:rnos
power:x:98:rnos
optical:x:990:rnos
scanner:x:991:rnos
rfkill:x:983:rnos
storage:x:988:rnos
audio:x:995:rnos
users:x:985:rnos
```

- Create `<custom_ISO_folder>/airootfs/etc/gshadow` file.
```
root:!*::root
rnos:!*::
sys:!!::rnos,bin
network:!!::rnos
power:!!::rnos
adm:!!::rnos
wheel:!!::rnos
uucp:!!::rnos
optical:!!::rnos
scanner:!!::rnos
rfkill:!!::rnos
video:!!::rnos,sddm
storage:!!::rnos
audio:!!::rnos
users:!!::rnos
```

- Copy the `sudoers` file from `/etc/sudoers` into `<custom_ISO_folder>/airootfs/etc`, and change its,
ownership with,
`$ sudo chown <username>:<username> <custom_ISO_folder>/airootfs/etc/sudoers`

- Make sure `<custom_ISO_folder>/etc/shadow` and `<custom_ISO_folder>/etc/gshadow` have the correct permissions.
```
...
file_permissions=(
  ...
  ["/etc/shadow"]="0:0:0400"
  ["/etc/gshadow"]="0:0:0400"
  ["/etc/sudoers"]="0:0:0400"
)
```

- For configuring `calamares`, refer to Ezarcher's calamares [video](https://www.youtube.com/watch?v=G6a2TpyrIhY),
after creating the `PKGBUILD` for it.

- The touchpad configuration is placed in `<custom_ISO_folder>/airootfs/etc/X11/xorg.conf.d/30-touchpad.conf`,
with the following contents,
```
Section "InputClass"
   Identifier "tap-by-default"
   MatchIsTouchpad "on"
   MatchDriver "libinput"
   Option "Tapping" "on"
   Option "NaturalScrolling" "true"
EndSection
```

- Place the qt5 settings, in `<custom_ISO_folder>/airootfs/root/.config/qt5ct` in a file named `qt5ct.conf`,
with the following contents (applies `JetBrains Mono` font to qt5 applications). Since, `calamares` is a `qt5`
application it will pick this up.
```
[Appearance]
color_scheme_path=/usr/share/qt5ct/colors/airy.conf
custom_palette=false
standard_dialogs=default
style=Fusion

[Fonts]
fixed=@Variant(\0\0\0@\0\0\0\x1c\0J\0\x65\0t\0\x42\0r\0\x61\0i\0n\0s\0 \0M\0o\0n\0o@\"\0\0\0\0\0\0\xff\xff\xff\xff\x5\x1\0\x32\x10)
general=@Variant(\0\0\0@\0\0\0\x1c\0J\0\x65\0t\0\x42\0r\0\x61\0i\0n\0s\0 \0M\0o\0n\0o@\"\0\0\0\0\0\0\xff\xff\xff\xff\x5\x1\0\x32\x10)

[Interface]
activate_item_on_single_click=1
buttonbox_layout=0
cursor_flash_time=1000
dialog_buttons_have_icons=1
double_click_interval=400
gui_effects=@Invalid()
keyboard_scheme=2
menus_have_icons=true
show_shortcuts_in_context_menus=true
stylesheets=@Invalid()
toolbutton_style=4
underline_shortcut=1
wheel_scroll_lines=3

[SettingsWindow]
geometry=@ByteArray(\x1\xd9\xd0\xcb\0\x3\0\0\0\0\x3\xc6\0\0\0!\0\0\au\0\0\x3\xba\0\0\x3\xc8\0\0\0#\0\0\as\0\0\x3\xb8\0\0\0\0\0\0\0\0\a\x80\0\0\x3\xc8\0\0\0#\0\0\as\0\0\x3\xb8)

[Troubleshooting]
force_raster_widgets=1
ignored_applications=@Invalid()
```

- For the GTK related settings, place the `~/.gtkrc-2.0` and `~/.config/gtk-3.0/settings/ini` in
`<custom_ISO_folder>/airootfs/etc/skel/` and `<custom_ISO_folder>/airootfs/etc/skel/.config/gtk-3.0/settings/.ini`
respectively. To set the default cursor theme, place `~/.icons/default/index.theme` in
`<custom_ISO_folder>/airootfs/etc/skel/.icons/default/index.theme`. All of this is also packaged up
as stated above.

- To make sure the `qt5` settings, and the cursor theme, are applied correctly, we need to create
environment variables for them, which can be put in `<custom_ISO_folder>/airootfs/etc/environment`,
with the following content,
```
#
# This file is parsed by pam_env module
#
# Syntax: simple "KEY=VAL" pairs on separate lines
#

QT_QPA_PLATFORMTHEME="qt5ct"
EDITOR=nvim
XCURSOR_THEME="Sunity-cursors"
```

- Since, `pkexec` does not pass many environment variables while execution of the command, we can pass them,
with the `env` option. Hence, to run calamares, with the desired font, and cursor theme, the `launch_calamares.sh`
script is used, that is called by the `welcome-app`. The calamares' desktop application also runs this script.
It's contents are as follows,
```
#!/bin/bash

# Author : Ruturajn <nanotiruturaj@gmail.com>

# This script launches `calamares`, using `pkexec` (all the environment variables are passed with `env` option).

pkexec env DISPLAY=$DISPLAY XAUTHORITY=$XAUTHORITY QT_QPA_PLATFORMTHEME=$QT_QPA_PLATFORMTHEME XCURSOR_THEME=$XCURSOR_THEME calamares
```

- Finally, you can build the ISO with,
```
$ mkarchiso -v -w /path/to/work_dir -o /path/to/out_dir /path/to/profile/
```
