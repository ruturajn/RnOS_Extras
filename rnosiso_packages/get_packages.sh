#!/bin/bash
# Author : Ruturajn <nanotiruturaj@gmail.com>

# This script creates a local custom database of packages for the Arch ISO.
# The packages are pulled from the AUR, as well as from local directories.

BYellow="\e[1;33m"
End_Colour="\e[0m"
BGreen="\e[1;32m"

echo -e "[ * ]${BYellow}Installing dependencies for the script, and ISO building tools${End_Colour}"
sudo pacman -Syu archiso devtools --needed

# Links for raw PKGBUILDs for all the packages
# GIT_LINK="https://aur.archlinux.org/lsd-bin.git"
# PKGBUILD_LINK="https://aur.archlinux.org/cgit/aur.git/plain/PKGBUILD?h=lsd-bin"
lsd_link="https://aur.archlinux.org/lsd-bin.git"
qtile_git_link="https://aur.archlinux.org/qtile-git.git"
qtile_extras_git_link="https://aur.archlinux.org/qtile-extras-git.git"
nerd_fonts_fantasque_sans_mono_link="https://aur.archlinux.org/nerd-fonts-fantasque-sans-mono.git"
ttf_jetbrains_mono_link="https://aur.archlinux.org/ttf-jetbrains-mono.git"
pipes_link="https://aur.archlinux.org/pipes.sh.git"
cava_link="https://aur.archlinux.org/cava.git"
brave_bin_link="https://aur.archlinux.org/brave-bin.git"
wpgtk_link="https://aur.archlinux.org/wpgtk.git"
picom_jonaburg_git_link="https://aur.archlinux.org/picom-jonaburg-git.git"
picom_ibhagwan_git_link="https://aur.archlinux.org/picom-ibhagwan-git.git"
sddm_theme_sugar_candy_git_link="https://aur.archlinux.org/sddm-theme-sugar-candy-git.git"
sunity_cursors_git_link="https://aur.archlinux.org/sunity-cursors-git.git"
catppuccin_gtk_theme_link="https://aur.archlinux.org/catppuccin-gtk-theme.git"
fm6000_link="https://aur.archlinux.org/fm6000.git"
ckbcomp_link="https://aur.archlinux.org/ckbcomp.git"
mkinitcpio_openswap_link="https://aur.archlinux.org/mkinitcpio-openswap.git"
yay_link="https://aur.archlinux.org/yay.git"
lf_link="https://aur.archlinux.org/lf.git"
i3lock_color_link="https://aur.archlinux.org/i3lock-color.git"
betterlockscreen_link="https://aur.archlinux.org/betterlockscreen.git"
tty_clock_git_link="https://aur.archlinux.org/tty-clock-git.git"
cbonsai_link="https://aur.archlinux.org/cbonsai.git"
fetchit_git_link="https://aur.archlinux.org/fetchit-git.git"

# Calamares config, and Welcome App for RnOS
rnos_calamares_config_link="https://gitlab.com/ruturajn/RnOS_Calamares_Config.git"
rnos_welcome_app_link="https://gitlab.com/ruturajn/RnOS_Welcome_App.git"
rnos_system_config_link="https://gitlab.com/ruturajn/RnOS_System_Config.git"

online_pkg_list=("${lsd_link}" "${qtile_git_link}" "${qtile_extras_git_link}" "${nerd_fonts_fantasque_sans_mono_link}"
	"${ttf_jetbrains_mono_link}" "${pipes_link}" "${cava_link}" "${brave_bin_link}" "${wpgtk_link}"
	"${picom_ibhagwan_git_link}" "${picom_jonaburg_git_link}" "${sddm_theme_sugar_candy_git_link}"
	"${sunity_cursors_git_link}" "${catppuccin_gtk_theme_link}" "${fm6000_link}" "${ckbcomp_link}"
	"${mkinitcpio_openswap_link}" "${rnos_calamares_config_link}" "${rnos_welcome_app_link}"
	"${rnos_system_config_link}" "${yay_link}" "${lf_link}" "${i3lock_color_link}" "${betterlockscreen_link}"
	"${tty_clock_git_link}" "${cbonsai_link}" "${fetchit_git_link}")

# Create the ./x86_64 directory
if [[ ! -d ./x86_64 ]]; then
	mkdir ./x86_64
fi

for link in "${online_pkg_list[@]}"; do
	pkgname=$(echo "${link}" | cut -d "/" -f 4 | sed 's|\.git||')
	echo -e "${BYellow}[ * ]Getting ${pkgname}${End_Colour}"
	git clone "${link}"
	if [[ ! -d "${pkgname}" ]]; then
		pkgname=$(echo "${link}" | cut -d "/" -f 5 | sed 's|\.git||')
	fi
	cd "${pkgname}" || exit
	if [[ "${pkgname}" == "qtile-extras-git" ]]; then
		sed -i 's|depends=("python" "qtile")|depends=("python" "qtile" "git" "python-setuptools" "python-wheel" "python-pip")|' PKGBUILD
	elif [[ "${pkgname}" == "ckbcomp" ]]; then
		sed -i 's|pkgver=1.208|pkgver=1.205|' PKGBUILD
		sed -i "s|sha512sums=.*|sha512sums=('SKIP')|" PKGBUILD
	elif [[ "${pkgname}" == "betterlockscreen" ]]; then
		sed -i "s|'i3lock-color' ||" PKGBUILD
	elif [[ "${pkgname}" == "tty-clock-git" ]]; then
		sed -i "s|depends=('ncurses')|depends=('ncurses' 'git')|" PKGBUILD
	elif [[ "${pkgname}" == "fetchit-git" ]]; then
		sed -i "s|pkgrel=1|pkgrel=1\ndepends=('git')|" PKGBUILD
	fi
	../build_package.sh
	if [[ "${pkgname}" == "RnOS_Calamares_Config" ]]; then
		pkgname="rnos-calamares-config"
	elif [[ "${pkgname}" == "RnOS_Welcome_App" ]]; then
		pkgname="rnos-welcome-app"
	elif [[ "${pkgname}" == "RnOS_System_Config" ]]; then
		pkgname="rnos-system-config"
	fi
	mv *"${pkgname}"*.pkg.tar.zst ../x86_64
	cd ../x86_64 || exit
	repo-add rnosiso_packages.db.tar.gz *"${pkgname}"*.pkg.tar.zst
	cd $(find ~ -name "rnosiso_packages" 2>/dev/null) || exit
	if [[ "${pkgname}" == "rnos-calamares-config" ]]; then
		pkgname="RnOS_Calamares_Config"
	elif [[ "${pkgname}" == "rnos-welcome-app" ]]; then
		pkgname="RnOS_Welcome_App"
	elif [[ "${pkgname}" == "rnos-system-config" ]]; then
		pkgname="RnOS_System_Config"
	fi
	sudo rm -rf "${pkgname}"
done

# Install packages from Local PKGBUILDs
fontawesome_python_path="./fontawesome-python"
calamares_path="./calamares"
#oh_my_fish_path="./oh-my-fish"
rnos_pfetch_path="./pfetch"

local_pkg_list=("${fontawesome_python_path}" "${calamares_path}" "${rnos_pfetch_path}")

for path in "${local_pkg_list[@]}"; do
	pkgname=$(echo "${path}" | sed 's|\.\/||')
	echo -e "${BYellow}[ * ]Getting ${pkgname}${End_Colour}"
	cd "${path}" || exit
	../build_package.sh
	mv *"${pkgname}"*.pkg.tar.zst ../x86_64
	if [[ "${pkgname}" == "calamares" || "${pkgname}" == "pfetch" ]]; then
		rm *"${pkgname}"*.tar.gz
	fi
	sudo rm -rf "${pkgname}"
	rm *.log
	cd ../x86_64 || exit
	repo-add rnosiso_packages.db.tar.gz *"${pkgname}"*.pkg.tar.zst
	cd ../ || exit
done
