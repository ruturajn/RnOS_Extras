# List of Plugins Required

- From the AUR
    - neovim-tree-lua-git [Installed]
    - neovim-web-devicons-git [Installed]
    - neovim-lspconfig [Installed]
    - neovim-luasnip-git [Installed]
    - neovim-notify-git [Installed] 
    - neovim-plenary-git 
    - neovim-tree-sitter-git [Installed]


- Packaged
    - [x] nvim-vim-shfmt-git
    - [x] nvim-vim-css-color-git
    - [x] nvim-catppuccin-git
    - [x] nvim-cmp-cmdline-git - Fixed
    - [x] nvim-lspkind-git
    - [x] nvim-barbar-git
    - [x] nvim-alpha-git
    - [x] nvim-lsp-installer-git
    - [x] nvim-vim-commentary-git
    - [x] nvim-vim-fugitive-git
    - [x] nvim-cmp-git - Fixed
    - [x] nvim-cmp-lsp-git - Fixed
    - [x] nvim-cmp-buffer-git - Fixed
    - [x] nvim-cmp-path-git - Fixed
    - [x] nvim-lualine-git
    - [x] nvim-telescope-git


- Install Language Servers using pacman,
    - lua-language-server
    - pyright
    - rust-analyzer
