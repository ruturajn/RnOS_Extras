#!/bin/bash

# Author : Ruturajn <nanotiruturaj@gmail.com>

# This script creates a local custom database of packages for the Arch ISO.
# The packages are pulled from the AUR, as well as from local directories.
# It packages up neovim plugins.

BYellow="\e[1;33m"
End_Colour="\e[0m"
BGreen="\e[1;32m"

echo -e "[ * ]${BYellow}Installing dependencies for the script, and ISO building tools${End_Colour}"
sudo pacman -Syu archiso devtools --needed

# Install Plugins from the AUR
neovim_plenary_git_link="https://aur.archlinux.org/neovim-plenary-git"
neovim_tree_lua_git_link="https://aur.archlinux.org/neovim-tree-lua-git.git"
neovim_web_devicons_git_link="https://aur.archlinux.org/neovim-web-devicons-git.git"
neovim_lspconfig_link="https://aur.archlinux.org/neovim-lspconfig.git"
neovim_luasnip_git_link="https://aur.archlinux.org/neovim-luasnip-git.git"
neovim_notify_git_link="https://aur.archlinux.org/neovim-notify-git.git"
# neovim_lualine_git_link="https://aur.archlinux.org/neovim-lualine-git.git"
neovim_tree_sitter_git_link="https://aur.archlinux.org/neovim-tree-sitter-git.git"
neovim_indent_blankline_link="https://aur.archlinux.org/neovim-indent-blankline.git"

aur_pkg_list=("${neovim_plenary_git_link}" "${neovim_tree_lua_git_link}" "${neovim_web_devicons_git_link}" "${neovim_lspconfig_link}"
	"${neovim_luasnip_git_link}" "${neovim_notify_git_link}" "${neovim_tree_sitter_git_link}" "${neovim_indent_blankline_link}")

# Create the ./x86_64 directory
if [[ ! -d ./x86_64 ]]; then
	mkdir ./x86_64
fi

for link in "${aur_pkg_list[@]}"; do
	pkgname=$(echo "${link}" | cut -d "/" -f 4 | sed 's|\.git||')
	echo -e "${BYellow}[ * ]Getting ${pkgname}${End_Colour}"
	git clone "${link}"
	cd "${pkgname}" || exit
	../build_package.sh
	mv *"${pkgname}"*.pkg.tar.zst ../x86_64
	cd ../x86_64 || exit
	repo-add rnosiso_packages.db.tar.gz *"${pkgname}"*.pkg.tar.zst
	cd $(find ~ -name "rnosiso_packages" 2>/dev/null) || exit
	sudo rm -rf "${pkgname}"
done

# Install packages from Local PKGBUILDs
catpuccin_nvim_path="./nvim-plugin-pkgbuilds/catppuccin-nvim-git"
nvim_alpha_git_path="./nvim-plugin-pkgbuilds/nvim-alpha-git"
nvim_barbar_git_path="./nvim-plugin-pkgbuilds/nvim-barbar-git"
nvim_cmp_buffer_git_path="./nvim-plugin-pkgbuilds/nvim-cmp-buffer-git"
nvim_cmp_git_path="./nvim-plugin-pkgbuilds/nvim-cmp-git"
nvim_cmp_lsp_git_path="./nvim-plugin-pkgbuilds/nvim-cmp-lsp-git"
nvim_cmp_path_git_path="./nvim-plugin-pkgbuilds/nvim-cmp-path-git"
nvim_lsp_installer_git_path="./nvim-plugin-pkgbuilds/nvim-lsp-installer-git"
nvim_lspkind_git_path="./nvim-plugin-pkgbuilds/nvim-lspkind-git"
nvim_telescope_git_path="./nvim-plugin-pkgbuilds/nvim-telescope-git"
nvim_vim_commentary_git_path="./nvim-plugin-pkgbuilds/nvim-vim-commentary-git"
nvim_vim_fugitive_git_path="./nvim-plugin-pkgbuilds/nvim-vim-fugitive-git"
vim_css_color_git_path="./nvim-plugin-pkgbuilds/vim-css-color-git"
vim_shfmt_git_path="./nvim-plugin-pkgbuilds/vim-shfmt-git"
nvim_lualine_git_path="./nvim-plugin-pkgbuilds/nvim-lualine-git"
nvim_cmp_cmdline_git_path="./nvim-plugin-pkgbuilds/nvim-cmp-cmdline-git"

local_pkg_list=("${catpuccin_nvim_path}" "${nvim_alpha_git_path}" "${nvim_barbar_git_path}"
	"${nvim_cmp_buffer_git_path}" "${nvim_cmp_git_path}" "${nvim_cmp_lsp_git_path}"
	"${nvim_cmp_path_git_path}" "${nvim_lsp_installer_git_path}" "${nvim_lspkind_git_path}"
	"${nvim_telescope_git_path}" "${nvim_vim_commentary_git_path}" "${nvim_vim_fugitive_git_path}"
	"${vim_css_color_git_path}" "${vim_shfmt_git_path}" "${nvim_lualine_git_path}" "${nvim_cmp_cmdline_git_path}")

for path in "${local_pkg_list[@]}"; do
	pkgname=$(cat "${path}"/PKGBUILD | head -n2 | tail -n1 | sed 's|^.*=||')
	echo -e "${BYellow}[ * ]Getting ${pkgname}${End_Colour}"
	cd "${path}" || exit
	../../build_package.sh
	mv *"${pkgname}"*.pkg.tar.zst ../../x86_64
	pkgname=$(cat ../../"${path}"/PKGBUILD | head -n3 | tail -n1 | sed 's|^.*=||')
	sudo rm -rf "${pkgname}"
	rm *.log
	cd ../../x86_64 || exit
	pkgname=$(cat ../"${path}"/PKGBUILD | head -n2 | tail -n1 | sed 's|^.*=||')
	repo-add rnosiso_packages.db.tar.gz *"${pkgname}"*.pkg.tar.zst
	cd ../ || exit
done
